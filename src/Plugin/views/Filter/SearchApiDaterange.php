<?php

namespace Drupal\search_api_daterange_filter\Plugin\views\Filter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Plugin\views\filter\SearchApiDate;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;

/**
 * Daterange views filter.
 *
 * Extend SearchApiDate filter to include date range operations.
 *
 * @ViewsFilter("search_api_daterange")
 */
class SearchApiDaterange extends SearchApiDate {

  /**
   * The end field.
   *
   * @var string
   */
  protected $endField;

  /**
   * The start date.
   *
   * @var \DateTimeInterface
   */
  protected $startDate;

  /**
   * The end date.
   *
   * @var \DateTimeInterface
   */
  protected $endDate;

  /**
   * {@inheritDoc}
   */
  public function operators() {
    $operators = parent::operators();
    $operators['includes'] = [
      'title' => $this->t('Includes'),
      'method' => 'opIncludes',
      'short' => $this->t('includes'),
      'values' => 1,
    ];
    $operators['past'] = [
      'title' => $this->t('Past'),
      'method' => 'opPast',
      'short' => $this->t('Past'),
      'values' => 1,
    ];
    $operators['upcoming'] = [
      'title' => $this->t('Upcoming'),
      'method' => 'opUpcoming',
      'short' => $this->t('Upcoming'),
      'values' => 1,
    ];

    return $operators;
  }

  /**
   * Filters by operator Includes.
   *
   * @param string $field
   *   The field name.
   */
  protected function opIncludes($field) {
    if ($this->checkField($field) === FALSE) {
      $this->opSimple($field);
      return;
    }

    // Set the start and end dates.
    $this->setDates();

    // Prepare or conditions.
    $or = $this->query->createConditionGroup('OR');
    // Date is between start and end date.
    $and = $this->query->createConditionGroup('AND');
    $and->addCondition($this->realField, $this->startDate, '<=');
    $and->addCondition($this->endField, $this->endDate, '>=');
    $or->addConditionGroup($and);

    // Start date is null and end date is greater than or equal to the date.
    $and = $this->query->createConditionGroup('AND');
    $and->addCondition($this->realField, NULL);
    $and->addCondition($this->endField, $this->endDate, '>=');
    $and->addCondition($this->realField, NULL);
    $and->addCondition($this->endField, $this->startDate, '<=');
    $or->addConditionGroup($and);

    // End date is null and start date is less than or equal to the date.
    $and = $this->query->createConditionGroup('AND');
    $and->addCondition($this->realField, $this->startDate, '<=');
    $and->addCondition($this->endField, NULL);
    $and->addCondition($this->realField, $this->endDate, '>=');
    $and->addCondition($this->endField, NULL);
    $or->addConditionGroup($and);

    // Add the condition group to the query.
    $this->query->addConditionGroup($or);
  }

  /**
   * Filters by operator Past.
   *
   * @param string $field
   *   The field name.
   */
  protected function opPast($field) {
    if ($this->checkField($field) === FALSE) {
      $this->opSimple($field);
      return;
    }

    // Set the start and end dates.
    $this->setDates();

    // Prepare or conditions.
    $or = $this->query->createConditionGroup('OR');
    // Date is less than or equal to start date and less than or equal to end date.
    $or->addCondition($this->endField, $this->endDate, '<');

    // Start date is null and end date is greater than or equal to the date.
    $and = $this->query->createConditionGroup();
    $and->addCondition($this->realField, $this->endDate, '<');
    $and->addCondition($this->endField, NULL);
    $or->addConditionGroup($and);

    $this->query->addConditionGroup($or);
  }

  /**
   * Filters by operator Upcoming.
   *
   * @param string $field
   *   The field name.
   */
  protected function opUpcoming($field) {
    if ($this->checkField($field) === FALSE) {
      $this->opSimple($field);
      return;
    }

    // Set the start and end dates.
    $this->setDates();

    // Prepare or conditions.
    $or = $this->query->createConditionGroup('OR');
    $or->addCondition($this->realField, $this->startDate, '>');

    // Start date is null and end date is greater than or equal to the date.
    $and = $this->query->createConditionGroup();
    $and->addCondition($this->realField, NULL);
    $and->addCondition($this->endField, $this->startDate, '>');
    $or->addConditionGroup($and);

    $this->query->addConditionGroup($or);
  }

  /**
   * Check if the field is a date range field.
   *
   * @param string $field
   *   The field name.
   *
   * @return bool
   *   TRUE if the field is a date range field, FALSE otherwise.
   */
  protected function checkField($field): bool {
    if (!$this->query instanceof SearchApiQuery) {
      $this->opSimple($field);
      return FALSE;
    }

    $this->setDateEndField();
    if ($this->endField === NULL) {
      $this->opSimple($field);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Set the start and end dates.
   */
  protected function setDates() {
    // Convert to ISO. UTC timezone is used since dates are stored in UTC.
    /** @var \Drupal\Component\Datetime\DateTimePlus $date */
    $date = new DateTimePlus($this->value['value'],
      new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->endDate = $date->getTimestamp();
    // Needs the day 23:59:59 timestamp.
    $date->modify('+86399 sec');
    $this->startDate = $date->getTimestamp();
  }

  /**
   * Return the end field identifier.
   */
  protected function setDateEndField(): void {
    $this->endField = NULL;
    if (!$this->query instanceof SearchApiQuery) {
      return;
    }
    /** @var \Drupal\search_api\Entity\Index $index */
    $index = $this->query->getIndex();
    /** @var \Drupal\search_api\Item\Field $original_field */
    $original_field = $index->getField($this->realField);
    if (!$original_field instanceof FieldInterface) {
      return;
    }

    // End date property path.
    $end_property_path = $original_field->getPropertyPath() . ':end_value';
    // Find the date end field.
    $filtered = array_filter($index->getFields(), function ($field) use ($end_property_path) {
      return $field->getPropertyPath() === $end_property_path;
    });
    if (!empty($filtered)) {
      // Set the first key as the end field.
      $this->endField = array_keys($filtered)[0] ?? NULL;
    }
  }

}
