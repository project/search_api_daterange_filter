## INTRODUCTION

The Search Api Daterange Filter module extend Search Api Date filter with
daterange option which filter data between start and end dates.

The primary use case for this module is:
- Craete a content with core daterange field.
- Create search api search index, add field date and field date end fields.
- Create search api index views with just date field filter do not add date end
field.
- In settings popup check exposed and use operator _**Includes**_.
- Optional use Better Exposed Filters module to make filter more user friendly.

## REQUIREMENTS

Drupal Search Api module

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
No configuration needed.

## MAINTAINER

Current maintainer for Drupal 10:

- Dudás József (dj1999) - https://www.drupal.org/u/dj1999
